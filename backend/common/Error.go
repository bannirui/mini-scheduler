// Package common @author dingrui @since 2021/11/18
package common

import (
	"errors"
)

var (
	LOCAL_IP_NOT_FOUND_ERR   = errors.New("本机网卡ip未找到")
	LOCK_ACQUIRE_ALREADY_ERR = errors.New("分布式锁已经被其他节点持有")
)

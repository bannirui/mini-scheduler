// Package common @author dingrui @since 2021/11/17
package common

const (
	// etcd中保存的任务目录
	JOB_SAVE_DIR = "/cron/jobs/"

	// 强杀任务的目录
	JOB_KILL_DIR = "/cron/kill/"

	// 任务锁目录
	JOB_LOCK_DIR = "/cron/lock/"

	// 执行器注册目录
	JOB_EXECUTOR_DIR = "/cron/executor/"

	// 保存任务事件
	JOB_SAVE_EVENT = 1

	// 删除任务事件
	JOB_DELETE_EVENT = 2

	// 强杀任务事件
	JOB_KILL_EVENT = 3

	// 任务日志存储mongo db
	MONGO_DB_NAME = "cron"

	// 任务日志存储mongo collection
	MONGO_COLLECTION_NAME = "log"

	// etcd中存储value为空值
	PUT_NULL_VALUE_TO_ETCD string = ""
)

// Package common @author dingrui @since 2021/11/17
package common

import (
	"strings"
)

// /cron/executor/1.1.1.1字符串中提取出ip地址
func ExtractIp(s string) string {
	return strings.TrimPrefix(s, JOB_EXECUTOR_DIR)
}

// /cron/kill/xxx提取出xxx
func ExtractJobNameFromKillKey(killKey string) string {
	return strings.TrimPrefix(killKey, JOB_KILL_DIR)
}

// /cron/jobs/xxx提取出xxx
func ExtractJobNameFromJobKey(jobKey string) string {
	return strings.TrimPrefix(jobKey, JOB_SAVE_DIR)
}

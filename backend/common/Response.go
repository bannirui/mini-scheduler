// Package common @author dingrui @since 2021/11/17
package common

import (
	"encoding/json"
)

type Response struct {
	Status bool        `json:"status"` // ture=成功 false=失败
	Msg    string      `json:"msg"`    // 异常信息
	Data   interface{} `json:"data"`   // 数据
}

// 返回响应
func BuildResponse(status bool, msg string, data interface{}) (resp []byte, err error) {
	var (
		response Response
	)
	response.Status = status
	response.Msg = msg
	response.Data = data
	// struct序列化json
	resp, err = json.Marshal(response)
	return
}

// 成功响应
func SuccessResponse(data interface{}) (resp []byte, err error) {
	return BuildResponse(true, "ok", data)
}

// 失败响应
func FailResponse(msg string, data interface{}) (resp []byte, err error) {
	return BuildResponse(false, msg, data)
}

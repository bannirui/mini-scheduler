// Package model @author dingrui @since 2021/11/18
package model

// 删除job任务
type DelJobDTO struct {
	Name string `json:"name"` // 任务名称
}

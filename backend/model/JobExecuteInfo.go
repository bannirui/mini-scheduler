// Package model @author dingrui @since 2021/11/18
package model

import (
	"context"
	"time"
)

// 任务执行状态
type JobExecuteInfo struct {
	Job        *Job               // 任务
	PlanTime   time.Time          // 理论的调度时间
	ActualTime time.Time          // 实际的调度时间
	CancelCtx  context.Context    // 任务command的context
	CancelFunc context.CancelFunc // 取消command执行的回调函数
}

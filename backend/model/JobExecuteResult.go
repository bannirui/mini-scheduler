// Package model @author dingrui @since 2021/11/18
package model

import (
	"time"
)

// 任务的执行结果
type JobExecuteResult struct {
	JobExecuteInfo *JobExecuteInfo // 执行状态
	Output         []byte          // 脚本输出
	Err            error           // 如果执行异常 存在的错误
	StartTime      time.Time       // 启动时间
	EndTime        time.Time       // 结束时间
}

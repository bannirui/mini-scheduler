// Package model @author dingrui @since 2021/11/18
package model

// 强杀job任务
type KillJobDTO struct {
	Name string `json:"name"` // 任务名称
}

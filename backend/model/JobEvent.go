// Package model @author dingrui @since 2021/11/18
package model

import (
	"time"

	"github.com/gorhill/cronexpr"
)

// etcd中任务的变化事件 watcher监听到的消息
type JobEvent struct {
	EventType int
	Job       *Job
}

// 构建任务执行计划
func (j *JobEvent) BuildJobSchedulePlan() (jobSchedulePlan *JobSchedulePlan, err error) {
	var (
		cronExpr *cronexpr.Expression
	)
	// cron表达式解析
	if cronExpr, err = cronexpr.Parse(j.Job.CronExpr); err != nil {
		goto ERR
	}
	jobSchedulePlan = &JobSchedulePlan{
		Job:      j.Job,
		CronExpr: cronExpr,
		NextTime: cronExpr.Next(time.Now()),
	}
	return
ERR:
	return
}

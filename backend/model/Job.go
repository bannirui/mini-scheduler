// Package model @author dingrui @since 2021/11/18
package model

import (
	"encoding/json"
)

// job任务
type Job struct {
	Name     string `json:"name"`     // 任务名称
	Command  string `json:"command"`  // 执行命令
	CronExpr string `json:"cronExpr"` // cron表达式
}

// etcd的value数据反序列化结构体
func DeserializeJob(data []byte) (job *Job, err error) {
	job = &Job{}
	err = json.Unmarshal(data, job)
	return
}

// 构建任务事件
func (j *Job) BuildJobEvent(eventType int) (jobEvent *JobEvent) {
	return &JobEvent{
		EventType: eventType,
		Job:       j,
	}
}

// Package model @author dingrui @since 2021/11/18
package model

import (
	"context"
	"time"

	"github.com/gorhill/cronexpr"
)

// 任务调度计划
type JobSchedulePlan struct {
	Job      *Job                 // 要调度的任务
	CronExpr *cronexpr.Expression // cron表达式解析结果
	NextTime time.Time            // 下次的调度时间
}

// 构建 执行状态信息
func (j *JobSchedulePlan) Convert2JobExecuteInfo() (jobExecuteInfo *JobExecuteInfo) {
	jobExecuteInfo = &JobExecuteInfo{
		Job:      j.Job,
		PlanTime: j.NextTime,
	}
	jobExecuteInfo.CancelCtx, jobExecuteInfo.CancelFunc = context.WithCancel(context.TODO())
	return
}

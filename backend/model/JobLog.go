// Package model @author dingrui @since 2021/11/18
package model

// 查询任务执行日志
type JobLogDTO struct {
	Name  string `json:"name"`  // 任务名称
	Skip  int    `json:"skip"`  // mongo skip
	Limit int    `json:"limit"` // mongo limit
}

// 任务日志
type JobLog struct {
	JobName      string `json:"jobName" bson:"jobName"`           // 任务名称
	Command      string `json:"command" bson:"command"`           // 脚本命令
	Err          string `json:"err" bson:"err"`                   // 执行错误详情
	Output       string `json:"output" bson:"output"`             // 控制台脚本输出
	PlanTime     int64  `json:"planTime" bson:"planTime"`         // 计划开始时间
	ScheduleTime int64  `json:"scheduleTime" bson:"scheduleTime"` // 实际调度时间
	StartTime    int64  `json:"startTime" bson:"startTime"`       // 任务执行开始时间
	EndTime      int64  `json:"endTime" bson:"endTime"`           // 任务执行结束时间
}

// mongo查询日志的过滤条件
type JobLogFilter struct {
	JobName string `bson:"jobName"` // 任务名称
}

// mongo日志查询的排序规则
type JobLogSortByStartTime struct {
	SortOrder int `bson:"sortTime"`
}

// 日志批次提交到mongo
type LogBatch struct {
	Logs []interface{} // 多条日志
}

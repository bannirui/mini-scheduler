// Package master @author dingrui @since 2021/11/17
package master

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/bannirui/mini-scheduler/backend/common"
	"github.com/bannirui/mini-scheduler/backend/model"
)

// 日志管理 mongodb存储
type LogMgr struct {
	collection *mongo.Collection
}

var (
	// 单例
	G_logMgr *LogMgr
)

// 初始化日志管理器
func InitLogMgr() (err error) {
	var (
		client     *mongo.Client
		collection *mongo.Collection
	)
	// mongo连接配置
	auth := options.Credential{
		Username:    G_config.MongodbUsername,
		Password:    G_config.MongodbPwd,
		PasswordSet: true,
	}
	if client, err = mongo.Connect(context.TODO(), options.Client().ApplyURI(G_config.MongodbUri).SetAuth(auth).SetConnectTimeout(time.Duration(G_config.MongodbConnectTimeout)*time.Second)); err != nil {
		goto ERR
	}
	// 连接校验
	if err = client.Ping(context.TODO(), nil); err != nil {
		goto ERR
	}
	collection = client.Database(common.MONGO_DB_NAME).Collection(common.MONGO_COLLECTION_NAME)
	// 单例赋值
	G_logMgr = &LogMgr{
		collection: collection,
	}
	return
ERR:
	fmt.Println(time.Now(), err)
	return
}

// 查看任务日志
func (l *LogMgr) listLog(name string, skip, limit int) (logArr []*model.JobLog, err error) {
	var (
		jobLogFilter *model.JobLogFilter
		findOpts     *options.FindOptions
		cursor       *mongo.Cursor
		jobLog       *model.JobLog
		limitConvert int64
		skipConvert  int64
	)
	// mongo查询条件
	jobLogFilter = &model.JobLogFilter{JobName: name}
	// 查询mongo
	limitConvert = int64(limit)
	skipConvert = int64(skip)
	findOpts = options.Find()
	findOpts.Limit = &limitConvert
	findOpts.Skip = &skipConvert
	// 按照任务的开始时间降序
	findOpts.Sort = &model.JobLogSortByStartTime{SortOrder: -1}
	if cursor, err = l.collection.Find(context.TODO(), jobLogFilter, findOpts); err != nil {
		goto ERR
	}
	// 释放资源
	defer cursor.Close(context.TODO())
	// 遍历
	for cursor.Next(context.TODO()) {
		jobLog = &model.JobLog{}
		// 反序列化
		if err = cursor.Decode(jobLog); err != nil {
			err = nil
			continue
		}
		logArr = append(logArr, jobLog)
	}
	return
ERR:
	fmt.Println(time.Now(), err)
	return
}

// Package master @author dingrui @since 2021/11/17
package master

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"time"

	"github.com/bannirui/mini-scheduler/backend/common"
	"github.com/bannirui/mini-scheduler/backend/model"
)

// http接口服务
type ApiService struct {
	httpServer *http.Server
}

var (
	// 单例对象
	G_apiService *ApiService
)

// 初始化http服务
func InitApiService() (err error) {
	var (
		mux        *http.ServeMux
		listener   net.Listener
		httpServer *http.Server
		address    string
	)
	mux = http.NewServeMux()
	// 路由
	mux.HandleFunc("/mini-scheduler/job/save", saveJobHandler)           // 新增任务
	mux.HandleFunc("/mini-scheduler/job/del", deleteJobHandler)          // 删除任务
	mux.HandleFunc("/mini-scheduler/job/list", listJobHandler)           // 查询所有任务
	mux.HandleFunc("/mini-scheduler/job/kill", killJobHandler)           // 强杀任务
	mux.HandleFunc("/mini-scheduler/job/log", jobLogHandler)             // 查询任务执行日志
	mux.HandleFunc("/mini-scheduler/executor/list", listExecutorHandler) // 查询所有执行器
	// 监听端口
	address = fmt.Sprintf(":%d", G_config.ApiPort)
	if listener, err = net.Listen("tcp", address); err != nil {
		return
	}
	// 创建http服务
	httpServer = &http.Server{
		ReadTimeout:  5000 * time.Millisecond,
		WriteTimeout: 5000 * time.Millisecond,
		Handler:      mux,
	}
	// 单例赋值
	G_apiService = &ApiService{httpServer: httpServer}
	// 启动http服务端
	go httpServer.Serve(listener)
	return
}

// 新增任务
// post
// /job/save
// {"name":"", "command":"", "cronExpr":""}
func saveJobHandler(w http.ResponseWriter, r *http.Request) {
	var (
		data        []byte
		err         error
		jobModel    model.Job // 任务
		oldJobModel *model.Job
		response    []byte
	)
	// 请求中读取body中json数据
	if data, err = ioutil.ReadAll(r.Body); err != nil {
		goto ERR
	}
	// json反序列化struct
	if err = json.Unmarshal(data, &jobModel); err != nil {
		goto ERR
	}
	// 保存任务
	if oldJobModel, err = G_jobMgr.SaveJob(&jobModel); err != nil {
		goto ERR
	}
	// 成功应答
	if response, err = common.SuccessResponse(oldJobModel); err == nil {
		_, _ = w.Write(response)
	}
	return
ERR:
	// 统一处理异常
	if response, err = common.FailResponse(err.Error(), nil); err == nil {
		_, _ = w.Write(response)
	}
}

// 删除任务
// post
// /job/del
// {"name":""}
func deleteJobHandler(w http.ResponseWriter, r *http.Request) {
	var (
		data      []byte
		err       error
		delJobDTO model.DelJobDTO
		oldJob    *model.Job
		response  []byte
	)
	// 请求中读取body中json数据
	if data, err = ioutil.ReadAll(r.Body); err != nil {
		goto ERR
	}
	// json反序列化struct
	if err = json.Unmarshal(data, &delJobDTO); err != nil {
		goto ERR
	}
	// 要删除的任务名称
	if oldJob, err = G_jobMgr.DelJob(delJobDTO.Name); err != nil {
		goto ERR
	}
	// 成功应答
	if response, err = common.SuccessResponse(oldJob); err == nil {
		_, _ = w.Write(response)
	}
	return
ERR:
	// 异常处理
	if response, err = common.FailResponse(err.Error(), nil); err == nil {
		_, _ = w.Write(response)
	}
}

// 查询所有任务
// get
// /job/list
func listJobHandler(w http.ResponseWriter, r *http.Request) {
	var (
		jobList  []*model.Job
		err      error
		response []byte
	)
	// 查询所有job
	if jobList, err = G_jobMgr.ListJob(); err != nil {
		goto ERR
	}
	// 成功
	if response, err = common.SuccessResponse(jobList); err == nil {
		_, _ = w.Write(response)
	}
	return
ERR:
	// 失败
	if response, err = common.FailResponse(err.Error(), nil); err == nil {
		_, _ = w.Write(response)
	}
}

// 根据任务名称强杀某个任务
// post
// /job/kill
func killJobHandler(w http.ResponseWriter, r *http.Request) {
	var (
		data       []byte
		err        error
		killJobDTO model.KillJobDTO
		response   []byte
	)
	// 解析请求中body数据
	if data, err = ioutil.ReadAll(r.Body); err != nil {
		goto ERR
	}
	// 反序列化
	if err = json.Unmarshal(data, &killJobDTO); err != nil {
		goto ERR
	}
	// 根据任务名称强杀任务
	if err = G_jobMgr.KillJob(killJobDTO.Name); err != nil {
		goto ERR
	}
	// 成功
	if response, err = common.SuccessResponse(nil); err == nil {
		_, _ = w.Write(response)
	}
	return
ERR:
	// 失败
	if response, err = common.FailResponse(err.Error(), nil); err == nil {
		_, _ = w.Write(response)
	}
}

// 查询任务日志
// post
// /job/log
func jobLogHandler(w http.ResponseWriter, r *http.Request) {
	// 请求体数据
	var (
		data      []byte
		err       error
		response  []byte
		jobLogDTO model.JobLogDTO
		logArr    []*model.JobLog
	)
	if data, err = ioutil.ReadAll(r.Body); err != nil {
		goto ERR
	}
	// 反序列化
	if err = json.Unmarshal(data, &jobLogDTO); err != nil {
		goto ERR
	}
	// 查询日志
	if logArr, err = G_logMgr.listLog(jobLogDTO.Name, jobLogDTO.Skip, jobLogDTO.Limit); err != nil {
		goto ERR
	}
	// 正常返回
	if response, err = common.SuccessResponse(logArr); err == nil {
		_, _ = w.Write(response)
	}
	return
ERR:
	if response, err = common.FailResponse(err.Error(), nil); err == nil {
		_, _ = w.Write(response)
	}
}

// 查询所有可用执行器
// get
// /executor/list
func listExecutorHandler(w http.ResponseWriter, r *http.Request) {
	var (
		executorArr []string
		err         error
		response    []byte
	)
	// 数组初始化
	executorArr = make([]string, 0)
	if executorArr, err = G_executorMgr.ListExecutor(); err != nil {
		goto ERR
	}
	if response, err = common.SuccessResponse(executorArr); err == nil {
		_, _ = w.Write(response)
	}
	return
ERR:
	if response, err = common.FailResponse(err.Error(), nil); err == nil {
		_, _ = w.Write(response)
	}
}

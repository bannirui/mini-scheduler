// Package master @author dingrui @since 2021/11/17
package master

import (
	"context"
	"time"

	"github.com/coreos/etcd/clientv3"
	"github.com/coreos/etcd/mvcc/mvccpb"

	"github.com/bannirui/mini-scheduler/backend/common"
)

// 执行器管理
type ExecutorMgr struct {
	client *clientv3.Client
	kv     clientv3.KV
	lease  clientv3.Lease
}

var (
	// 单例
	G_executorMgr *ExecutorMgr
)

// 执行器管理器初始化
func InitExecutorMgr() (err error) {
	var (
		config clientv3.Config
		client *clientv3.Client
		kv     clientv3.KV
		lease  clientv3.Lease
	)
	// etcd客户端连接配置
	config = clientv3.Config{
		Endpoints:   G_config.EtcdEndpoints,
		DialTimeout: time.Duration(G_config.EtcdDialTimeout) * time.Second,
	}
	// etcd连接
	if client, err = clientv3.New(config); err != nil {
		goto ERR
	}
	// kv和lease的api子集
	kv = clientv3.NewKV(client)
	lease = clientv3.NewLease(client)
	// 单例赋值
	G_executorMgr = &ExecutorMgr{
		client: client,
		kv:     kv,
		lease:  lease,
	}
	return
ERR:
	return
}

// 获取在线executor列表
func (e *ExecutorMgr) ListExecutor() (executorArr []string, err error) {
	var (
		getResponse *clientv3.GetResponse
		kvPair      *mvccpb.KeyValue
		executorIp  string
	)
	// 获取etcd目录下所有的kv
	if getResponse, err = e.kv.Get(context.TODO(), common.JOB_EXECUTOR_DIR, clientv3.WithPrefix()); err != nil {
		goto ERR
	}
	// 初始化数组
	executorArr = make([]string, 0)
	// 解析每个value中的ip
	for _, kvPair = range getResponse.Kvs {
		// key=/cron/executor/1.1.1.1
		executorIp = common.ExtractIp(string(kvPair.Key))
		executorArr = append(executorArr, executorIp)
	}
	return
ERR:
	return
}

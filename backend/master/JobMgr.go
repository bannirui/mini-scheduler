// Package master @author dingrui @since 2021/11/17
package master

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/coreos/etcd/clientv3"
	"github.com/coreos/etcd/mvcc/mvccpb"

	"github.com/bannirui/mini-scheduler/backend/common"
	"github.com/bannirui/mini-scheduler/backend/model"
)

type JobMgr struct {
	kv    clientv3.KV
	lease clientv3.Lease
}

var (
	// 单例
	G_jobMgr *JobMgr
)

func InitJobMgr() (err error) {
	var (
		config clientv3.Config
		client *clientv3.Client
		kv     clientv3.KV
		lease  clientv3.Lease
	)
	// etcd客户端连接配置
	config = clientv3.Config{
		Endpoints:   G_config.EtcdEndpoints,
		DialTimeout: time.Duration(G_config.EtcdDialTimeout) * time.Second,
	}
	// etcd连接
	if client, err = clientv3.New(config); err != nil {
		goto ERR
	}
	// kv和lease的api子集
	kv = clientv3.NewKV(client)
	lease = clientv3.NewLease(client)
	// 单例赋值
	G_jobMgr = &JobMgr{
		kv:    kv,
		lease: lease,
	}
	return
ERR:
	fmt.Println(err)
	return
}

// 保存任务
func (j *JobMgr) SaveJob(job *model.Job) (oldJob *model.Job, err error) {
	var (
		jobKey      string
		jobValue    []byte
		putResponse *clientv3.PutResponse
		oldJobObj   model.Job
	)
	// 存储在etcd中的key
	jobKey = common.JOB_SAVE_DIR + job.Name
	// 序列化json
	if jobValue, err = json.Marshal(job); err != nil {
		goto ERR
	}
	// 保存etcd
	if putResponse, err = j.kv.Put(context.TODO(), jobKey, string(jobValue), clientv3.WithPrevKV()); err != nil {
		goto ERR
	}
	// 可能是新增也可能是更新 更新操作将旧值返回
	if putResponse.PrevKv != nil {
		// 旧值反序列化
		if err = json.Unmarshal(putResponse.PrevKv.Value, &oldJobObj); err != nil {
			err = nil
			return
		}
		oldJob = &oldJobObj
	}
	return
ERR:
	return
}

// 根据名称删除任务
func (j *JobMgr) DelJob(jobName string) (oldJob *model.Job, err error) {
	var (
		jobKey         string
		deleteResponse *clientv3.DeleteResponse
		oldJobObj      model.Job
	)
	// job的key
	jobKey = common.JOB_SAVE_DIR + jobName
	// 删除
	if deleteResponse, err = j.kv.Delete(context.TODO(), jobKey, clientv3.WithPrevKV()); err != nil {
		goto ERR
	}
	// 返回被删除的任务
	if len(deleteResponse.PrevKvs) != 0 {
		// 反序列化旧值
		if err = json.Unmarshal(deleteResponse.PrevKvs[0].Value, &oldJobObj); err != nil {
			err = nil
			return
		}
		oldJob = &oldJobObj
	}
	return
ERR:
	return
}

// 查询所有任务
func (j *JobMgr) ListJob() (jobList []*model.Job, err error) {
	var (
		getResponse *clientv3.GetResponse
		kvPair      *mvccpb.KeyValue
		job         *model.Job
	)
	// etcd中目录下所有的任务
	if getResponse, err = j.kv.Get(context.TODO(), common.JOB_SAVE_DIR, clientv3.WithPrefix()); err != nil {
		goto ERR
	}
	// 数组初始化
	jobList = make([]*model.Job, 0)
	// 遍历查询到的所有任务
	for _, kvPair = range getResponse.Kvs {
		job = &model.Job{}
		// 反序列化
		if err = json.Unmarshal(kvPair.Value, job); err != nil {
			// 序列化出错直接忽略
			err = nil
			continue
		}
		jobList = append(jobList, job)
	}
	return
ERR:
	return
}

// 根据任务名称强杀任务
func (j *JobMgr) KillJob(jobName string) (err error) {
	var (
		leaseGrantResponse *clientv3.LeaseGrantResponse
	)
	// 申请一个租约 让所有executor监听到一次put操作 创建一个租约自动过期
	if leaseGrantResponse, err = j.lease.Grant(context.TODO(), 1); err != nil {
		goto ERR
	}
	// 获取租约id 跟kv绑定起来
	if _, err = j.kv.Put(context.TODO(), common.JOB_KILL_DIR+jobName, common.PUT_NULL_VALUE_TO_ETCD, clientv3.WithLease(leaseGrantResponse.ID)); err != nil {
		goto ERR
	}
	return
ERR:
	return
}

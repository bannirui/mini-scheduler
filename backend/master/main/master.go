package main

import (
	"flag"
	"fmt"
	"runtime"
	"time"

	"github.com/bannirui/mini-scheduler/backend/master"
)

var (
	CONFIG_FILE_PATH string // 配置文件路径
)

func main() {
	var (
		err error
	)
	// 初始化线程
	initEnv()
	// 初始化命令行参数
	initArgs()
	// 配置加载
	if err = master.InitConfig(CONFIG_FILE_PATH); err != nil {
		goto ERR
	}
	// 初始化master的各个模块
	// 执行器管理器
	if err = master.InitExecutorMgr(); err != nil {
		goto ERR
	}
	// 日志管理器
	if err = master.InitLogMgr(); err != nil {
		goto ERR
	}
	// 任务管理器
	if err = master.InitJobMgr(); err != nil {
		goto ERR
	}
	// 启动http服务 对外暴露api接口
	if err = master.InitApiService(); err != nil {
		goto ERR
	}
	fmt.Println(time.Now(), "mini-scheduler master start success")
	// cpu空转不退出main线程
	for {
		time.Sleep(1 * time.Second)
	}
ERR:
	// 异常统一处理
	fmt.Println(err)
}

// 初始化线程
func initEnv() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

// 解析命令行参数
func initArgs() {
	const (
		flagSpecifiedName string = "config"
		flagDefaultValue  string = "./master.json"
		flagUsage         string = "指定master.json"
	)
	// master -h
	// master -config ./master.json
	flag.StringVar(&CONFIG_FILE_PATH, flagSpecifiedName, flagDefaultValue, flagUsage)
	flag.Parse()
}

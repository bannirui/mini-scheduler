package main

import (
	"flag"
	"fmt"
	"runtime"
	"time"

	"github.com/bannirui/mini-scheduler/backend/executor"
)

var (
	CONFIG_FILE_PATH string // 配置文件路径
)

func main() {
	var (
		err error
	)
	// 初始化线程
	initEnv()
	// 初始化命令行参数
	initArgs()
	// 配置加载
	if err = executor.InitConfig(CONFIG_FILE_PATH); err != nil {
		goto ERR
	}
	// 初始化executor的各个模块
	// 工作节点注册
	if err = executor.InitRegister(); err != nil {
		goto ERR
	}
	// 日志
	if err = executor.InitLogHandler(); err != nil {
		goto ERR
	}
	// 执行器
	if err = executor.InitExecutor(); err != nil {
		goto ERR
	}
	// 调度器
	if err = executor.InitScheduler(); err != nil {
		goto ERR
	}
	// 任务管理器
	if err = executor.InitJobMgr(); err != nil {
		goto ERR
	}
	fmt.Println(time.Now(), "mini-scheduler worker start success")
	// cpu空转不退出main线程
	for {
		time.Sleep(1 * time.Second)
	}
ERR:
	// 异常统一处理
	fmt.Println(err)
	return
}

// 初始化线程
func initEnv() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

// 解析命令行参数
func initArgs() {
	const (
		flagSpecifiedName string = "config"
		flagDefaultValue  string = "./executor.json"
		flagUsage         string = "指定executor.json"
	)
	// master -h
	// master -config ./executor.json
	flag.StringVar(&CONFIG_FILE_PATH, flagSpecifiedName, flagDefaultValue, flagUsage)
	flag.Parse()
}

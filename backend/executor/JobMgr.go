// Package executor @author dingrui @since 2021/11/18
package executor

import (
	"context"
	"fmt"
	"time"

	"github.com/coreos/etcd/clientv3"
	"github.com/coreos/etcd/mvcc/mvccpb"

	"github.com/bannirui/mini-scheduler/backend/common"
	"github.com/bannirui/mini-scheduler/backend/model"
)

// 任务管理器
type JobMgr struct {
	client  *clientv3.Client
	kv      clientv3.KV
	lease   clientv3.Lease
	watcher clientv3.Watcher
}

var (
	// 单例
	G_jobMgr *JobMgr
)

// 初始化
func InitJobMgr() (err error) {
	var (
		config  clientv3.Config
		client  *clientv3.Client
		kv      clientv3.KV
		lease   clientv3.Lease
		watcher clientv3.Watcher
	)
	// etcd客户端连接配置
	config = clientv3.Config{
		Endpoints:   G_config.EtcdEndpoints,
		DialTimeout: time.Duration(G_config.EtcdDialTimeout) * time.Second,
	}
	// etcd连接
	if client, err = clientv3.New(config); err != nil {
		goto ERR
	}
	// api子集 kv lease watcher
	kv = clientv3.NewKV(client)
	lease = clientv3.NewLease(client)
	watcher = clientv3.NewWatcher(client)
	// 单例赋值
	G_jobMgr = &JobMgr{
		client:  client,
		kv:      kv,
		lease:   lease,
		watcher: watcher,
	}
	// 监听任务
	G_jobMgr.watchJob()
	// 监听kill
	G_jobMgr.watchKill()
	return
ERR:
	fmt.Println(err)
	return
}

// 监听任务 /cron/jobs/xxx
func (j *JobMgr) watchJob() {
	var (
		getResponse        *clientv3.GetResponse
		err                error
		watchStartRevision int64
		kvPair             *mvccpb.KeyValue
		job                *model.Job
		watchChan          clientv3.WatchChan
		watchResponse      clientv3.WatchResponse
		watchEvent         *clientv3.Event
		jobName            string
	)
	// 根据前缀获取当前etcd目录下所有的任务 拿到当前集群的revision
	if getResponse, err = j.kv.Get(context.TODO(), common.JOB_SAVE_DIR, clientv3.WithPrefix()); err != nil {
		goto ERR
	}
	// revision
	watchStartRevision = getResponse.Header.Revision + 1
	// 遍历出所有的key value
	for _, kvPair = range getResponse.Kvs {
		if job, err = model.DeserializeJob(kvPair.Value); err == nil {
			// 构建任务事件推送给scheduler
			fmt.Println(time.Now(), "任务管理器监听etcd任务 推送任务事件给调度器 jobName=", job.Name)
			G_scheduler.PushJobEvent(job.BuildJobEvent(common.JOB_SAVE_EVENT))
		} else {
			// todo 能put进etcd的kv理论上就一定能正确反序列化
			fmt.Println(err)
		}
	}
	// 监听目录的后序变化
	go func() {
		watchChan = j.watcher.Watch(context.TODO(), common.JOB_SAVE_DIR, clientv3.WithRev(watchStartRevision))
		// 处理监听到的事件
		for watchResponse = range watchChan {
			for _, watchEvent = range watchResponse.Events {
				switch watchEvent.Type {
				case mvccpb.PUT:
					// 任务保存
					// etcd数据反序列化为job 根据任务构建一个任务事件派发给scheduler
					if job, err = model.DeserializeJob(watchEvent.Kv.Value); err == nil {
						G_scheduler.PushJobEvent(job.BuildJobEvent(common.JOB_SAVE_EVENT))
					}
				case mvccpb.DELETE:
					// 任务删除
					jobName = common.ExtractJobNameFromJobKey(string(watchEvent.Kv.Key))
					// 创建job
					job = &model.Job{Name: jobName}
					// 根据job构建任务事件推送给scheduler
					G_scheduler.PushJobEvent(job.BuildJobEvent(common.JOB_DELETE_EVENT))
				}
			}
		}
	}()

	return
ERR:
	fmt.Println(err)
	return
}

// 监听强杀 /cron/kill/xxx
func (j *JobMgr) watchKill() {
	var (
		watchChan     clientv3.WatchChan
		watchResponse clientv3.WatchResponse
		watchEvent    *clientv3.Event
		jobName       string
		job           *model.Job
		jobEvent      *model.JobEvent
	)
	// 启动协程监听etcd的/cron/kill目录变化 往这个目录写的kv键值对绑定了一个ttl=1s的lease租约 过来一个需要强杀的任务 就等于一个put事件+删除事件 只关注put事件即可
	go func() {
		watchChan = j.watcher.Watch(context.TODO(), common.JOB_KILL_DIR, clientv3.WithPrefix())
		// 处理监听事件
		for watchResponse = range watchChan {
			for _, watchEvent = range watchResponse.Events {
				switch watchEvent.Type {
				case mvccpb.PUT:
					// 客户端put过来kv
					jobName = common.ExtractJobNameFromKillKey(string(watchEvent.Kv.Key))
					// 只需要用任务名称构建出来任务事件发送给调度器就行
					job = &model.Job{
						Name: jobName,
					}
					jobEvent = job.BuildJobEvent(common.JOB_KILL_EVENT)
					// 任务事件推送给调度器scheduler
					fmt.Println(time.Now(), "任务管理器监听etcd任务 强杀 推送任务事件给调度器 jobName=", job.Name)
					G_scheduler.PushJobEvent(jobEvent)
				case mvccpb.DELETE:
					// delete事件 lease租约过期了 触发了kv删除事件
					// todo
				}
			}
		}
	}()
	return
}

// 创建分布式锁 key=任务名称
func (j *JobMgr) CreateJobLock(jobName string) (JobLock *JobLock) {
	JobLock = InitJobLock(jobName, j.kv, j.lease)
	return
}

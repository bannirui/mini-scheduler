// Package executor @author dingrui @since 2021/11/17
package executor

import (
	"encoding/json"
	"io/ioutil"
)

// executor配置项
type Config struct {
	EtcdEndpoints         []string `json:"etcdEndpoints"`         // etcd的集群列表
	EtcdDialTimeout       int      `json:"etcdDialTimeout"`       // etcd的连接超时
	MongodbUri            string   `json:"mongodbUri"`            // mongodb地址
	MongodbUsername       string   `json:"mongodb_username"`      // mongo用户名
	MongodbPwd            string   `json:"mongodbPwd"`            // mongo密码
	MongodbConnectTimeout int      `json:"mongodbConnectTimeout"` // mongodb连接超时时间
	MongoCommitBatchSize  int      `json:"mongoCommitBatchSize"`  // 提交批次大小
	MongoCommitTimeout    int      `json:"mongoCommitTimeout"`    // 提交超时
}

var (
	// 单例
	G_config *Config
)

// 加载配置
func InitConfig(fileName string) (err error) {
	var (
		data []byte
		conf Config
	)
	// 读取配置项
	if data, err = ioutil.ReadFile(fileName); err != nil {
		goto ERR
	}
	// 反序列化
	if err = json.Unmarshal(data, &conf); err != nil {
		goto ERR
	}
	// 单例赋值
	G_config = &conf
	return
ERR:
	return
}

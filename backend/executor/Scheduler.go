// Package executor @author dingrui @since 2021/11/18
package executor

import (
	"fmt"
	"time"

	"github.com/bannirui/mini-scheduler/backend/common"
	"github.com/bannirui/mini-scheduler/backend/model"
)

// 任务调度器
type Scheduler struct {
	jobEventChan      chan *model.JobEvent              // watch etcd中的变化事件
	jobPlanTable      map[string]*model.JobSchedulePlan // 任务调度计划表
	jobExecutingTable map[string]*model.JobExecuteInfo  // 任务执行表
	jobResultChan     chan *model.JobExecuteResult      // 任务执行结果队列
}

var (
	// 单例
	G_scheduler *Scheduler
)

// 调度器初始化
func InitScheduler() (err error) {
	// 单例赋值
	G_scheduler = &Scheduler{
		jobEventChan:      make(chan *model.JobEvent, 1000),
		jobPlanTable:      make(map[string]*model.JobSchedulePlan),
		jobExecutingTable: make(map[string]*model.JobExecuteInfo),
		jobResultChan:     make(chan *model.JobExecuteResult, 1000),
	}
	// 启动协程
	go G_scheduler.scheduleLoop()
	return
}

// 调度协程
func (s *Scheduler) scheduleLoop() {
	var (
		scheduleAfter    time.Duration
		scheduleTimer    *time.Timer
		jobEvent         *model.JobEvent
		jobExecuteResult *model.JobExecuteResult
	)
	// 尝试初始化
	scheduleAfter = s.TrySchedule()
	// 调度的延迟定时器
	scheduleTimer = time.NewTimer(scheduleAfter)
	// 轮询 定时任务
	for {
		select {
		case jobEvent = <-s.jobEventChan: // 监听到任务的变化事件(新增 更新 删除)
			// 对内存中维护的任务列表做对应的curd更新
			fmt.Println(time.Now(), "调度器收到一个任务管理器分发过来的任务事件 jobName=", jobEvent.Job.Name)
			s.handleJobEvent(jobEvent)
		case <-scheduleTimer.C: // 任务到期了
		case jobExecuteResult = <-s.jobResultChan: // 监听到任务的执行结果
			fmt.Println(time.Now(), "调度器收到一个执行器上传过来的任务执行结果 jobName=", jobEvent.Job.Name)
			s.handleJobResult(jobExecuteResult)
		}
		// 调度一次任务
		scheduleAfter = s.TrySchedule()
		// 重置调度时间间隔
		scheduleTimer.Reset(scheduleAfter)
	}
}

// 处理监听到的任务的事件
func (s *Scheduler) handleJobEvent(jobEvent *model.JobEvent) {
	var (
		jobSchedulePlan        *model.JobSchedulePlan
		err                    error
		jobSchedulePlanExisted bool // 任务计划是否存在计划表中
		jobExecuteInfo         *model.JobExecuteInfo
		jobExecuting           bool // 任务正在执行中
	)
	// 根据监听的事件类型执行对应的逻辑
	switch jobEvent.EventType {
	case common.JOB_SAVE_EVENT:
		// 保存任务事件(新增 更新)
		// 构建任务计划
		if jobSchedulePlan, err = jobEvent.BuildJobSchedulePlan(); err != nil {
			return
		}
		// 加到任务执行计划表中
		s.jobPlanTable[jobEvent.Job.Name] = jobSchedulePlan
	case common.JOB_DELETE_EVENT:
		// 任务删除事件
		// 该任务是否在计划表中
		if jobSchedulePlan, jobSchedulePlanExisted = s.jobPlanTable[jobEvent.Job.Name]; jobSchedulePlanExisted {
			// 存在计划表中 从计划表中移除
			delete(s.jobPlanTable, jobEvent.Job.Name)
		}
	case common.JOB_KILL_EVENT:
		// 任务强杀事件
		// 执行中的人取消命令执行
		if jobExecuteInfo, jobExecuting = s.jobExecutingTable[jobEvent.Job.Name]; jobExecuting {
			// 回调执行取消函数 触发command杀死shell进程
			jobExecuteInfo.CancelFunc()
		}
	}
}

// 处理任务的执行结果
func (s *Scheduler) handleJobResult(jobExecuteResult *model.JobExecuteResult) {
	var (
		jobLog *model.JobLog
	)
	// 删除执行状态 将该任务从正在执行的map中移除
	delete(s.jobExecutingTable, jobExecuteResult.JobExecuteInfo.Job.Name)
	// 生成执行日志
	if jobExecuteResult.Err != common.LOCK_ACQUIRE_ALREADY_ERR {
		jobLog = &model.JobLog{
			JobName:      jobExecuteResult.JobExecuteInfo.Job.Name,
			Command:      jobExecuteResult.JobExecuteInfo.Job.Command,
			Output:       string(jobExecuteResult.Output),
			PlanTime:     jobExecuteResult.JobExecuteInfo.PlanTime.UnixNano() / 1000 / 1000,
			ScheduleTime: jobExecuteResult.JobExecuteInfo.ActualTime.UnixNano() / 1000 / 1000,
			StartTime:    jobExecuteResult.StartTime.UnixNano() / 1000 / 1000,
			EndTime:      jobExecuteResult.EndTime.UnixNano() / 1000 / 1000,
		}
		if jobExecuteResult.Err != nil {
			jobLog.Err = jobExecuteResult.Err.Error()
		} else {
			jobLog.Err = ""
		}
		// 执行日志保存
		G_logHandler.Append(jobLog)
	}
}

// 调度任务 计算任务的调度状态
func (s *Scheduler) TrySchedule() (scheduleAfter time.Duration) {
	var (
		now      time.Time
		jobPlan  *model.JobSchedulePlan
		nearTime *time.Time
	)
	// 如果任务表为空 slepp
	if len(s.jobPlanTable) == 0 {
		scheduleAfter = 1 * time.Second
		return
	}
	// 任务表不为空 进行调度
	// 获取当前时间
	now = time.Now()
	// 遍历出任务表中所有的任务job
	for _, jobPlan = range s.jobPlanTable {
		// 任务的执行时间已经过期 代表可以执行任务 任务执行时间<=当前时间
		if !jobPlan.NextTime.After(now) {
			// 执行任务
			s.TryStartJob(jobPlan)
			// 更新任务的下次执行时间
			jobPlan.NextTime = jobPlan.CronExpr.Next(now)
		}
		// 最近一个即将要过期的任务时间
		if nearTime == nil || jobPlan.NextTime.Before(*nearTime) {
			// 找出所有任务里面最小的执行时间
			nearTime = &jobPlan.NextTime
		}
	}
	// 下次的调度间隔=最近要执行的任务调度时间-当前时间
	scheduleAfter = (*nearTime).Sub(now)
	return
}

// 执行任务
func (s *Scheduler) TryStartJob(jobPlan *model.JobSchedulePlan) {
	var (
		jobExecuteInfo      *model.JobExecuteInfo
		jobExecuteInfoExist bool // job存在正在执行的map中 表示当前任务正在执行
	)
	// 任务已经在执行了就放弃再次执行
	if jobExecuteInfo, jobExecuteInfoExist = s.jobExecutingTable[jobPlan.Job.Name]; jobExecuteInfoExist {
		// 当前任务正在执行中
		return
	}
	// 当前任务还没执行
	// 构建执行状态
	jobExecuteInfo = jobPlan.Convert2JobExecuteInfo()
	// 缓存到正在执行的状态表中
	s.jobExecutingTable[jobPlan.Job.Name] = jobExecuteInfo
	// 执行当前任务
	G_executor.ExecuteJob(jobExecuteInfo)
}

// 将任务事件推送给调度器
func (s *Scheduler) PushJobEvent(event *model.JobEvent) {
	s.jobEventChan <- event
}

// 将任务执行结果推送给调度器
func (s *Scheduler) pushJobResult(result *model.JobExecuteResult) {
	s.jobResultChan <- result
}

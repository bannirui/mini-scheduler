// Package executor @author dingrui @since 2021/11/18
package executor

import (
	"context"

	"github.com/coreos/etcd/clientv3"

	"github.com/bannirui/mini-scheduler/backend/common"
)

// 分布式锁实现
type JobLock struct {
	kv         clientv3.KV
	lease      clientv3.Lease
	jobName    string             // key
	cancelFunc context.CancelFunc // 回调终止租约lease的自动续租
	leaseId    clientv3.LeaseID
	isLocked   bool // 是否上锁成功
}

// 上锁
func (l *JobLock) TryLock() (err error) {
	var (
		leaseGrantResponse     *clientv3.LeaseGrantResponse
		leaseCancelCtx         context.Context
		leaseCancelFunc        context.CancelFunc // 回调取消租约自动续租
		leaseKeepAliveResponse <-chan *clientv3.LeaseKeepAliveResponse
		leaseId                clientv3.LeaseID // 租约id
		txn                    clientv3.Txn
		lockKey                string
		txnResponse            *clientv3.TxnResponse
	)
	// 申请租约lease
	if leaseGrantResponse, err = l.lease.Grant(context.TODO(), 5); err != nil {
		// 申请失败
		goto LEASE_FAIL
	}
	leaseId = leaseGrantResponse.ID
	// context
	leaseCancelCtx, leaseCancelFunc = context.WithCancel(context.TODO())
	// 开启自动续租
	if leaseKeepAliveResponse, err = l.lease.KeepAlive(leaseCancelCtx, leaseId); err != nil {
		// 自动续约异常 需要释放租约
		goto LOCK_FAIL
	}

	// 开启协程处理续租的应答
	go func() {
		var (
			keepResponse *clientv3.LeaseKeepAliveResponse
		)
		for {
			select {
			case keepResponse = <-leaseKeepAliveResponse:
				if keepResponse == nil {
					// 没有收到续租应答就退出这个方法
					goto END
				}
			}
		}
	END:
		// 退出for轮询
	}()

	// 创建事务
	txn = l.kv.Txn(context.TODO())
	// 上锁的key
	lockKey = common.JOB_LOCK_DIR + l.jobName
	// 事务抢锁
	txn.
		If(clientv3.Compare(clientv3.CreateRevision(lockKey), "=", 0)).
		Then(clientv3.OpPut(lockKey, common.PUT_NULL_VALUE_TO_ETCD, clientv3.WithLease(leaseId))).
		Else(clientv3.OpGet(lockKey))
	// 提交事务
	if txnResponse, err = txn.Commit(); err != nil {
		// 上锁失败
		err = common.LOCK_ACQUIRE_ALREADY_ERR
		goto LOCK_FAIL
	}
	// 判断事务是否执行成功
	if !txnResponse.Succeeded {
		// 分布式锁已经被其他节点持有
		err = common.LOCK_ACQUIRE_ALREADY_ERR
		goto LOCK_FAIL
	}
	// 当前节点竞争锁成功
	l.isLocked = true
	l.leaseId = leaseId
	l.cancelFunc = leaseCancelFunc
	return

LEASE_FAIL:
	// 上锁失败-申请租约失败
	// todo 根据业务定制重试机制 减少cpu压力
	return
LOCK_FAIL:
	// 上锁失败-事务抢锁失败(锁已经被别的节点持有) 此刻已经申请到租约了 需要释放租约
	// 取消自动续约
	leaseCancelFunc()
	// 释放租约
	_, _ = l.lease.Revoke(context.TODO(), leaseId)
	return
}

// 锁释放
func (l *JobLock) UnLock() {
	if l.isLocked {
		// 取消lease的自动续租 但是这样还要等待ttl才能释放kv
		l.cancelFunc()
		// 释放租约 跟lease绑定的kv也会立即释放 达到立即释放锁的效果
		_, _ = l.lease.Revoke(context.TODO(), l.leaseId)
	}
}

// 初始化锁
func InitJobLock(jobName string, kv clientv3.KV, lease clientv3.Lease) (jobLock *JobLock) {
	return &JobLock{
		kv:      kv,
		lease:   lease,
		jobName: jobName,
	}
}

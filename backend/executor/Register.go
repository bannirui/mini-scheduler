// Package executor @author dingrui @since 2021/11/18
package executor

import (
	"context"
	"fmt"
	"net"
	"time"

	"github.com/coreos/etcd/clientv3"

	"github.com/bannirui/mini-scheduler/backend/common"
)

// 节点注册到etcd
type Register struct {
	client *clientv3.Client
	kv     clientv3.KV
	lease  clientv3.Lease
	ip     string // 本机ip
}

var (
	// 单例
	G_register *Register
)

// 初始化
func InitRegister() (err error) {
	var (
		config clientv3.Config
		client *clientv3.Client
		kv     clientv3.KV
		lease  clientv3.Lease
		ip     string // 当前工作节点的ip地址
	)
	// etcd客户端连接配置
	config = clientv3.Config{
		Endpoints:   G_config.EtcdEndpoints,
		DialTimeout: time.Duration(G_config.EtcdDialTimeout) * time.Second,
	}
	// etcd连接
	if client, err = clientv3.New(config); err != nil {
		goto ERR
	}
	// kv和lease的api子集
	kv = clientv3.NewKV(client)
	lease = clientv3.NewLease(client)
	// 当前工作节点ip
	if ip, err = getLocalIp(); err != nil {
		goto ERR
	}
	// 单例赋值
	G_register = &Register{
		client: client,
		kv:     kv,
		lease:  lease,
		ip:     ip,
	}
	// 将工作节点注册到etcd
	go G_register.register()
	return
ERR:
	fmt.Println(err)
	return
}

// 获取本机ip地址
func getLocalIp() (ip string, err error) {
	var (
		addrArr []net.Addr
		addr    net.Addr
		ipNet   *net.IPNet // ip地址
		isIpNet bool
	)
	// 获取所有网卡
	if addrArr, err = net.InterfaceAddrs(); err != nil {
		goto ERR
	}
	// 获取第一个非lo的网卡ip
	for _, addr = range addrArr {
		// 网络地址 ipv4 ipv6
		if ipNet, isIpNet = addr.(*net.IPNet); isIpNet && !ipNet.IP.IsLoopback() {
			// 只取ipv4
			if ipNet.IP.To4() != nil {
				ip = ipNet.IP.String()
				return
			}
		}
	}
	// 遍历完都没有找到本机的ip地址
	err = common.LOCAL_IP_NOT_FOUND_ERR
	return
ERR:
	fmt.Println(err)
	return
}

// 将工作节点注册到etcd集群 自动续租
func (r *Register) register() {
	var (
		err                error
		leaseGrantResponse *clientv3.LeaseGrantResponse
		cancelFunc         context.CancelFunc
		keepAliveChan      <-chan *clientv3.LeaseKeepAliveResponse
		cancelCtx          context.Context
		keepAliveResponse  *clientv3.LeaseKeepAliveResponse
	)
	// 轮询
	for {
		// 取消回调函数重置为空
		cancelFunc = nil
		// 申请lease租约
		if leaseGrantResponse, err = r.lease.Grant(context.TODO(), 10); err != nil {
			// 申请失败 进行重试
			goto LEASE_RETRY
		}
		// 租约自动续租
		if keepAliveChan, err = r.lease.KeepAlive(context.TODO(), leaseGrantResponse.ID); err != nil {
			goto LEASE_RETRY
		}
		cancelCtx, cancelFunc = context.WithCancel(context.TODO())
		// 绑定kv put到etcd
		if _, err = r.kv.Put(cancelCtx, common.JOB_EXECUTOR_DIR+r.ip, common.PUT_NULL_VALUE_TO_ETCD, clientv3.WithLease(leaseGrantResponse.ID)); err != nil {
			// put失败 重试
			goto LEASE_RETRY
		}
		// fmt.Println(time.Now(), "节点注册成功 ip: ", r.ip)
		// kv已经put成功 进行租约续租的应答处理
		for {
			select {
			case keepAliveResponse = <-keepAliveChan:
				if keepAliveResponse == nil {
					// 续租失败
					goto LEASE_RETRY
				}
				// fmt.Println(time.Now(), "节点注册续租 ip: ", r.ip)
			}
		}

	LEASE_RETRY:
		// 休眠后继续
		time.Sleep(1 * time.Second)
		if cancelFunc != nil {
			cancelFunc()
		}
	}
}

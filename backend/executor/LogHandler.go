// Package executor @author dingrui @since 2021/11/18
package executor

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/bannirui/mini-scheduler/backend/common"
	"github.com/bannirui/mini-scheduler/backend/model"
)

// 日志处理器
type LogHandler struct {
	client         *mongo.Client
	collection     *mongo.Collection
	logChan        chan *model.JobLog   // 任务的日志 持久化数据结构
	autoCommitChan chan *model.LogBatch // 批次提交
}

var (
	// 单例
	G_logHandler *LogHandler
)

// 初始化
func InitLogHandler() (err error) {
	var (
		client *mongo.Client
	)
	// mongo连接配置
	auth := options.Credential{
		Username:    G_config.MongodbUsername,
		Password:    G_config.MongodbPwd,
		PasswordSet: true,
	}
	if client, err = mongo.Connect(context.TODO(), options.Client().ApplyURI(G_config.MongodbUri).SetAuth(auth).SetConnectTimeout(5*time.Second)); err != nil {
		goto ERR
	}
	// 连接校验
	if err = client.Ping(context.TODO(), nil); err != nil {
		goto ERR
	}
	// 单例赋值
	G_logHandler = &LogHandler{
		client:         client,
		collection:     client.Database(common.MONGO_DB_NAME).Collection(common.MONGO_COLLECTION_NAME),
		logChan:        make(chan *model.JobLog, 1000),
		autoCommitChan: make(chan *model.LogBatch, 1000),
	}
	// 启动协程处理mongo
	go G_logHandler.writeLoop()
	return
ERR:
	fmt.Println(err)
	return
}

// 日志存储mongo
func (l *LogHandler) writeLoop() {
	var (
		jobLog          *model.JobLog
		logBatch        *model.LogBatch
		commitTimer     *time.Timer     // 定时器
		timeoutLogBatch *model.LogBatch // 过期的批次
	)
	// 轮询
	for {
		select {
		case jobLog = <-l.logChan:
			if logBatch == nil {
				logBatch = &model.LogBatch{}
				// 让该批次超时自动提交
				time.AfterFunc(
					time.Duration(G_config.MongoCommitTimeout)*time.Millisecond,
					func(logBatch *model.LogBatch) func() {
						return func() {
							l.autoCommitChan <- logBatch
						}
					}(logBatch),
				)
			}
			// 新记录追加到批次中
			logBatch.Logs = append(logBatch.Logs, jobLog)
			// 批次中数量达到阈值就发送
			if len(logBatch.Logs) >= G_config.MongoCommitBatchSize {
				// 存储
				l.saveRecords(logBatch)
				// 清空
				logBatch = nil
				// 取消定时器
				commitTimer.Stop()
			}
		case timeoutLogBatch = <-l.autoCommitChan:
			// 过期的批次是否是当前的批次
			if timeoutLogBatch != logBatch {
				continue
			}
			// 提交到mongo
			l.saveRecords(timeoutLogBatch)
			// 清空
			logBatch = nil
		}
	}
}

// mongo批量写
func (l *LogHandler) saveRecords(batch *model.LogBatch) {
	_, _ = l.collection.InsertMany(context.TODO(), batch.Logs)
}

// 发送记录(一条任务执行日志)
func (l *LogHandler) Append(record *model.JobLog) {
	select {
	case l.logChan <- record:
	default:
		// 队列已经满了 记录写不进去 丢弃
	}
}

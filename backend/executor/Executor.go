// Package executor @author dingrui @since 2021/11/18
package executor

import (
	"fmt"
	"math/rand"
	"os/exec"
	"time"

	"github.com/bannirui/mini-scheduler/backend/model"
)

// 执行器
type Executor struct {
}

var (
	// 单例
	G_executor *Executor
)

// 初始化
func InitExecutor() (err error) {
	// 单例赋值
	G_executor = &Executor{}
	return
}

// 执行任务
func (e *Executor) ExecuteJob(jobExecuteInfo *model.JobExecuteInfo) {
	go func() {
		var (
			result  *model.JobExecuteResult // 任务执行结果
			jobLock *JobLock                // 分布式锁
			err     error
			cmd     *exec.Cmd
			output  []byte
		)
		// 存储任务的执行结果
		result = &model.JobExecuteResult{
			JobExecuteInfo: jobExecuteInfo,
			Output:         make([]byte, 0), // 初始化容器
		}
		// 初始化分布式锁
		jobLock = G_jobMgr.CreateJobLock(jobExecuteInfo.Job.Name)
		// 任务开始时间
		result.StartTime = time.Now()
		// 防止整个executor集群时钟不一致导致锁竞争不公平 随机sleep(0...1s)
		time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
		// 上锁
		err = jobLock.TryLock()
		// 锁释放
		defer jobLock.UnLock()
		if err != nil {
			// 上锁失败
			result.Err = err
			result.EndTime = time.Now()
		} else {
			// 持锁成功
			result.StartTime = time.Now()
			// 执行shell命令
			cmd = exec.CommandContext(jobExecuteInfo.CancelCtx, "/bin/bash", "-c", jobExecuteInfo.Job.Command)
			// 捕获控制台输出
			output, err = cmd.CombinedOutput()
			fmt.Printf("时间=%v jobName=%s 控制台命令=%s 控制台输出=%s 执行异常=%v", time.Now(), jobExecuteInfo.Job.Name, jobExecuteInfo.Job.Command, string(output), err)
			// 任务执行结束事件
			result.EndTime = time.Now()
			result.Output = output
			result.Err = err
		}
		// 任务执行完成后将执行结果推送给scheduler
		G_scheduler.pushJobResult(result)
	}()
}

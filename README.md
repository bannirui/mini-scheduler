## mini-scheduler

> 单点crontab定时任务

* 极度依赖服务器运行状态，服务器宕机服务就停止了运行

* 单机资源无法支撑大数量服务需求

* 执行状态查询不透明

> 分布式架构

* 调度器 高可用
* 执行器 高扩展

> 架构

![架构](./docs/assets/架构.png)



> 版本

```shell
golang=1.7
npm=8.1.3
vue-cli=4.5.15
etcd image=3.5.1-debian-10-r26
mongo image=5.0.3
```

> 运行

```shell
master节点
go run ./backend/master/master.go -config {absPath}/master.json

worker节点
go run ./backend/master/executor.go -config {absPath}/executor.json

web
cd /front
npm run serve
```

> demo
![demo](./docs/assets/demo.png)
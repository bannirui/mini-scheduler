import AxiosService from "@/utils/axios";
import {Response} from "@/api/response";
import {ref} from "vue";

// 任务执行日志
export interface JobLog {
    // 任务名称
    jobName: string;
    // 脚本命令
    command: string;
    // 执行错误详情
    err: string;
    // 控制台脚本输出
    output: string;
    // 计划开始时间
    planTime: bigint;
    // 实际调度时间
    scheduleTime: bigint;
    // 任务执行开始时间
    startTime: bigint;
    // 任务执行结束时间
    endTime: bigint;
}

// 查询任务的执行日志
export interface ListJobLogResponse extends Response{
    data: [JobLog]
}

// 查询任务执行日志的请求body
export interface ListJobLogRequest {
    // 任务名称
    name: string;
    // 分页参数skip
    skip: number;
    // 分页参数limit
    limit: number;
}

export class LogService {
    // 查询任务的日志
    static async listLogByJobName(jobName: string, skip: number, limit: number): Promise<ListJobLogResponse> {
        // 请求body
        const request:ListJobLogRequest={
            name: jobName,
            skip: skip,
            limit: limit,
        };
        const {data: res} = await AxiosService(
            "/job/log",
            {
                method: "post",
                data: request,
            }
        );
        return res;
    }
}

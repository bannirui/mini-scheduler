import AxiosService from "@/utils/axios"
import {Response} from "@/api/response"
import {Request} from "@/api/request";

export interface Job {
    name: string;
    command: string;
    cronExpr: string;
}

export interface ListJobResponse extends Response {
    data: [Job];
}

export interface SaveJobResponse extends Response {
    data: Job;
}

export interface UpdateJobResponse extends Response {
    data: Job;
}

export interface DeleteJobRequest extends Request {
    name: string;
}

export interface DeleteJobResponse extends Response {

}

export interface KillJobRequest extends Request {
    name: string;
}

export interface KillJobResponse extends Response {

}

export class JobService {
    // 查询所有任务
    static async listJob(): Promise<ListJobResponse> {
        const {data: res} = await AxiosService(
            "/job/list",
            {
                method: "get",
            }
        );
        return res;
    };
    // 添加任务
    static async saveJob(request: Job): Promise<SaveJobResponse> {
        const {data: res} = await AxiosService(
            "/job/save",
            {
                method: "post",
                data: request,
            }
        );
        return res;
    };
    // 修改任务
    static async updateJob(request: Job): Promise<UpdateJobResponse> {
        const {data: res} = await AxiosService(
            "/job/save",
            {
                method: "post",
                data: request,
            }
        );
        return res;
    };
    // 删除任务
    static async deleteJob(request: DeleteJobRequest): Promise<DeleteJobResponse> {
        const {data: res} = await AxiosService(
            "/job/del",
            {
                method: "post",
                data: request,
            }
        );
        return res;
    };
    // 强杀任务
    static async killJob(request: KillJobRequest): Promise<KillJobResponse> {
        const {data: res} = await AxiosService(
            "/job/kill",
            {
                method: "post",
                data: request,
            }
        );
        return res;
    };
}

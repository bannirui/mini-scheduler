module.exports = {
    devServer: {
        open: true,
        host: 'localhost',
        port: 8080,
        https: false,
        hotOnly: false,
        proxy: {
            '/dev-api': {
                target: 'http://localhost:8081/mini-scheduler/',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/dev-api': ''
                }
            }
        },
        before: app => {}
    }
}
